package com.example.arturolopez.los_pebles1;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "main_activity";
    private Button chatButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        chatButton = (Button) findViewById(R.id.chat_button);


        chatButton.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                Toast.makeText(getApplicationContext(),"pressed chat",Toast.LENGTH_SHORT).show();
                //i need you to fix this...
                //the toast works, but it crahses wit the following
                Intent i = new Intent(MainActivity.this, ChatActivity.class);

                if(i == null){
                    Log.d(TAG, "intent null");
                    Toast.makeText(getApplicationContext(), "intent null", Toast.LENGTH_SHORT).show();
                }
                else if(i != null){
                    startActivity(i);
                }
            }
        });


    }




}
